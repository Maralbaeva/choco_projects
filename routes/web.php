<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PostController@index');
Route::get('index2', 'PostController@func');
Route::get('add', 'PostController@addpost');


Route::get('home', 'HomeController@index')->name('home');
Route::get('users', 'PostController@users');

Route::get('qa', 'PostController@qa');

Route::get('questions_add', 'PostController@qa_add');
Route::post('questions_add', 'PostController@qa_post');

Route::get('qa/answer/{id}', 'PostController@qa_answer');
Route::post('qa/answer/{id}', 'PostController@qa_answer_post');

Route::get('logout', function(){
    Auth::logout();
    return redirect('/');
});
Auth::routes();
/*Route::get('/','PostController@index' {
	return view('index');
});
Route::get('/posts', 'PostController@show' {
	return view('index2');
});*/
?>