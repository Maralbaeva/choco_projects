<?php
namespace App\Http\Controllers;
use App\Post;
use App\Feedback;
use App\User;
use App\Question;
use Illuminate\Http\Request;
// use MongoClient;
class PostController extends Controller
{
    public function index(){

        $posts = Post::all();
        return view('index', compact ('posts'));
        //return json_encode($posts);
    }

    /*public function show(){
    	$post = Post::all()->toArray();
		//json_decode($post, true);
    	$out = array_values($post);
		json_encode($out);
		dd ($out);

        //$hidden = ['post'];
        //$converted = ($post) ? 'true' : 'false';
        //$converted = json_encode($post);
        //json_decode(string $converted[,bool $assoc=false [,int $depth=512 [,int $options=0]]]);
		//dd($converted);
    }*/
    /*public function func(){
        $fun = Feedback::all();
        return view('index', compact ('posts'));
    }*/
    public function func(){
        $fun = Post::all();
        return view('index2', compact ('fun'));
    }

    public function addpost(){
        $add = Post::all();
        return view('add', compact('add'));
    }
    public function users(){
        $users = User::paginate(10);
        return view('users', compact('users'));
    }
     public function qa(){
        $questions = Question::paginate(10);
        return view('questions', compact('questions'));
    }

    public function qa_add(){
        return view('questions_add');
    }
    public function qa_post(Request $request){

        $q = new Question();
        $q->question = $request->input('question');
        $q->name = $request->input('name');
        if($request->input('ano') == 'on'){
            $q->user_id = 1;
        }else{
            $q->user_id = 0;
        }
        $q->email = $request->input('email');
        $q->answer = "";
        $q->save();
        
        return redirect('qa');
    }

    public function qa_answer($id){
        $question = Question::find($id);
        return view('questions_answer', compact('question'));
    }

    public function qa_answer_post($id, Request $request){
        $question = Question::find($id);
        $question->answer = $request->answer;
        $question->save();

        return redirect('qa');
    }

}