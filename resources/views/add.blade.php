<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Создание акции</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
</head>
<body>
    <div>
        <nav class="navbar navbar-default">
            <div class="row">
              <div class="container-fluid col-md-5 col-md-offset-2">
                <ul class="nav navbar-nav menu">
                  <li class="active"><a href="index.html">Chocolife</a></li>
                  <li><a href="#">Chocomart</a></li>
                  <li><a href="#">Chocotravel</a></li>
                  <li><a href="#">Lensmark</a></li>
                  <li><a href="#">Chocofood</a></li>
                </ul>
              </div>
            </div>
        </nav>
        <div class="mainadd" style="height: 520px; padding-left: 15%; padding-right: 15%;">
            <div class="row">
                <div class="col-md-3"><img src="images/705.png"></div>
                <p style="font-size: 20px; margin-top: 10px;"><b>Создать акцию</b></p>
            </div>
            <div class="bod" style="padding-left: 10%; padding-right: 10%; height: 440px; margin-top: 20px;" >
                <div class="panel panel-warning class" style="height: 400px;">
                    <div class="panel-body">
                        <div class="form-group">
                          <label for="usr">Название акции:</label>
                          <input type="text" class="form-control" id="usr">
                        </div>
                        <div class="form-group">
                          <label for="comment">Описание акции:</label>
                          <textarea class="form-control" rows="5" id="comment"></textarea>
                        </div> 
                        <div class="form-group">
                          <label for="usr">Цена:</label>
                          <input type="text" class="form-control" id="usr">
                        </div>
                        <div>
                            <input name="myFile" type="file">
                        </div>
                        <button type="button" class="btn btn-success" style="right: 18px; position: absolute;">Готово</button>
                    </div>
                </div>
            </div>
        </div>

        
        
        <div class="footer">
            <div class="row">
                <div class="col-md-2"><h4 style="color: white;">Компания</h4><hr><a>О chocolife.me</a></br><a>Пресса о нас</a></br><a>Работай с нами</a></br><a>Контакты</a></div>
                <div class="col-md-2"><h4 style="color: white;">Клиентам</h4><hr><a>Обратная связь</a></br><a>Обучающий видеоролик</a></br><a>Вопросы и ответы</a></br><a>Публичная оферта</a></div>
                <div class="col-md-2"><h4 style="color: white;">Партнерам</h4><hr><a>Для вашего бизнеса</a></div>
                <div class="col-md-4 col-md-offset-2"><h4 style="color: white;">Наше приложение</h4><hr><p style="color: white;">Chocolife.me теперь еще удобнее и всегда под рукой!</p>
                    <img src="images/googleplay.png" style="float: left;">
                    <img src="images/appstore.png" style="margin-left: 3px;">
                </div>
            </div>
            <hr>
            <div class="row">
                <p style="color: white;">Chocolife.me | 2011-2017 - <a>Карта сайта</a></p>
            </div>
        </div>
    </div>
</body>
</html>