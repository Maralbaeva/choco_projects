@extends('layouts.app')

@section('content')

	<style type="text/css">
        table tr td{
            padding: 15px;
        }
        table thead tr td{
            background: #ececec;
        }
        .pagination{
            margin: 25px 0 25px 0;
        }
        .pagination li{
            float: left;
            padding: 10px;
            margin-right: 15px;
        }
    </style>

    <div class="localnav-wrapper localnav-headless gh-show-below">
        <div class="localnav"></div>
    </div>
	
	 <div id="page">
        <div class="container">
            <h1>Questions</h1>
			<table border="1" style="width: 100%; margin-bottom: 50px;">
				<thead>
					<tr>
						<td>#</td>
						<td>Вопрос</td>
						<td>Ответ</td>
						<td>Пользователь</td>
					</tr>
				</thead>

				<tbody>
					@foreach($questions as $q)
						<tr>
							<td>{{ $q->id }}</td>
							<td>
								{{ $q->question }}
								@if(Auth::user()->name == 'admin')
									<br><a href="{{ url('qa/answer/'.$q->id) }}">Answer</a>
								@endif
							</td>
							<td>{{ (!empty($q->answer)) ? $q->answer : 'Нет ответа' }}</td>
							<td>{{ ($q->user_id == 1) ? $q->name : 'Анонимно'  }}</td>
						</tr>
					@endforeach
				</tbody>

			</table>
			{{ $questions->links() }}
        </div>
     </div>

@endsection