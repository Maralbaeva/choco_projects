@extends('layouts.app')

@section('content')
    <div class="localnav-wrapper localnav-headless gh-show-below">
        <div class="localnav"></div>
    </div>

    <div id="page">
        <div class="container">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

                <p>You are logged in!</p>
                <a href="{{ url('users') }}">Watch info about our users!</a>
        </div>
    </div>
@endsection
