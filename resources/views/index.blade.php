<?php
$connection = mysqli_connect('localhost', 'root', '', 'chocolife');
session_start();
$id=1;
$ses_sql = mysqli_query($connection,"select id, image, info, price from posts where id='$id'");
$row = mysqli_fetch_assoc($ses_sql);
$id_session = $row['id'];
$image_session = $row['image'];
$info_session = $row['info'];
$price_session = $row['price'];
?>
@extends('layouts.app')

@section('class') page-home @endsection

@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Купоны, скидки и акции в Казахстане: все скидочные купоны в одном месте на сайте Chocolife.me</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
</head>
<body>
    <div>
        <nav class="navbar navbar-default">
            <div class="row">
              <div class="container-fluid col-md-5 col-md-offset-2">
                <ul class="nav navbar-nav menu">
                  <li class="active"><a href="#">Chocolife</a></li>
                  <li><a href="#">Chocomart</a></li>
                  <li><a href="#">Chocotravel</a></li>
                  <li><a href="#">Lensmark</a></li>
                  <li><a href="#">Chocofood</a></li>
                </ul>
              </div>
              <button type="button" class="btn btn-warning col-md-1 col-md-offset-1 reg">Регистрация</button>
              <h6 class="col-md-1 enter"><a>Вход</a></h6>
              <h6 class="col-md-1 enter"><a href="/add">Создать акцию</a></h6>
            </div>
        </nav>
        <div class="main">
            <div class="row">
                <div class="col-md-3"><img src="images/705.png" />
                </div>
                <div class="col-md-2"><h5>Главное,чтобы Вы </br> были счастливы!</h5></div>
                <div class="col-md-7">
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Найти среди 748 акций">
                        </div>
                    <button type="submit" class="btn btn-warning search">Поиск</button>
                    </form>
                </div>
            </div>
            <span><img src="images/cine.png"></img></span>
            <div class="row">
            <hr>
                <nav class="menu"> 
                    <div class="col-md-offset-7" id="menu">
                        <ul class="third">
                            <li><a href="#">Все</a></li>
                            <li><a href="#">Новые</a></li>
                            <li><a href="#" style="color: red;">Хиты продаж</a></li>
                            <li><a href="#">Развлечения и отдых</a></li>
                            <li><a href="#">Красота и здоровье</a></li>
                            <li><a href="#">Спорт</a></li>
                            <li><a href="#">Товары</a></li>
                            <li><a href="#">Услуги</a></li>
                            <li><a href="#">Еда</a></li>
                            <li><a href="#">Туризм,отели</a></li>
                            <li><a href="#" style="color: green;"><b>Бесплатные купоны</b></a></li>
                            <hr>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="row ad">
                <div class="col-md-3 pic" style="background-image: url(images/1.png);">
                </div>
                <div class="col-md-3 pic" style="background-image: url(images/2.png);">
                </div>
                <div class="col-md-3 pic" style="background-image: url(images/3.png);">
                </div>
                <div class="col-md-3 pic" style="background-image: url(images/4.png);">
                </div>
            </div>

            <div class="row sort">
                <table class="tabl">
                    <tr>
                        <td>Сортировать: </td>
                        <td>
                            <input type="radio" name="order_type" value="popular"/> <label>популярные</label>
                        </td>
                        <td>
                            <input type="radio" name="order_type" value="price" /> <label>цена</label>
                        </td>
                        <td>
                            <input type="radio" name="order_type" value="discount"/> <label>скидка</label>
                        </td>
                        <td>
                            <input type="radio" name="order_type" value="new" /> <label>новые</label>
                        </td>
                        <td>
                            <input type="radio" name="order_type" value="rating" data-t="1" data-c="1" /><label>рейтинг</label>
                        </td>
                        </tr>
                </table>
            </div>

            <div class="row first">
                <div class="col-md-4 pict" style="padding-left: 0;"><a href="/index2"><img src="images/11.png"></a>    
                </div>
                <div class="col-md-4 pict" style="background-image: url(images/12.png); background-repeat: no-repeat;"></div>
                <div class="col-md-4 pict" style="background-image: url(images/13.png); background-repeat: no-repeat;"></div>
            </div>
            <div class="row second">
                <div class="col-md-4 pict" style="background-image: url(images/11.png); background-repeat: no-repeat;"></div>
                <div class="col-md-4 pict" style="background-image: url(images/12.png); background-repeat: no-repeat;"></div>
                <div class="col-md-4 pict" style="background-image: url(images/13.png); background-repeat: no-repeat;"></div>
            </div>
            <div class="row third">
                <div class="col-md-4 pict" style="background-image: url(images/11.png); background-repeat: no-repeat;"></div>
                <div class="col-md-4 pict" style="background-image: url(images/12.png); background-repeat: no-repeat;"></div>
                <div class="col-md-4 pict" style="background-image: url(images/13.png); background-repeat: no-repeat;"></div>
            </div>

            </div>
        <div class="info">
            <div class="inf">
                    <img src="images/1.jpg" style="float: left; margin-right: 20px; margin-bottom: 10px;"></img>
                    <h5>ВСЕ СКИДКИ И АКЦИИ В ОДНОМ МЕСТЕ!</h5>
                    <p style="font-size: 12px;">Человеку для счастья нужно совсем мало: оставаться здоровым, иметь крепкую семью и хорошую работу, хранить гармонию души. Но иногда это не все. Не хватает какого-то приятного дополнения, например, получить бешеную скидку на популярную услугу. И этот бонус можем подарить вам именно мы! Благодаря купонам, приобретенным в нашем сервисе, вы сможете стать самым счастливым человеком в любой точке города Алматы.
    Мы предлагаем скидки в Алматы на самые разнообразные виды деятельности и услуги, которые распределены по таким рубрикам:</p>
                    <p style="font-size: 12px;"> -полная подборка (все самые актуальные); </br>
                        -новые (самые свежие предложения);</br>
                        -санатории (отдых и лечение в наиболее престижных санаториях); </br>
                        -красота (услуги салонов красоты, а также отдельных мастеров); </br>
                        -здоровье и спорт (услуги спортзалов, фитнес-клубов и т. д.);</br>
                        -еда (рестораны, кондитерские, суши, доставка и т. д.);</br>
                        -развлечения (караоке, сауна и т. д.);</br>
                        -услуги (авто-услуги, свадебные услуги и т. д.);</br>
                        -товары (для школы, для дома и т. д.);</br>
                        -отдых (туризм, гостиницы, отели и т. д.).</br>
                    <h5>ПОКУПАЙТЕ КУПОНЫ И ЭКОНОМЬТЕ НА УСЛУГАХ И ТОВАРАХ!</h5>
                    <p style="font-size: 12px;">В нашем купонном сервисе Шоколайф можно приобрести горящие скидки и купоны в Алматы с самым разным акционным диапазоном: от 30% до 90%. На сайте представлено более 530 разных возможностей для реализации личных планов. Например, вы можете в очередной раз посетить «Sarafan cafe», но теперь со скидочным купоном на 50% оставить там намного меньшую сумму. Такой купон обойдется вам всего в 399 тенге.
    Срок действия каждой акции в Алматы указан на нашем сайте. Эта информация поможет приобрести купон для посещения вами не только интересного предложения, но и оптимального во времени. Чтобы быстрее отыскать нужную акцию в нашем агрегаторе скидок, воспользуйтесь удобным поиском сайта онлайн по всем имеющимся рубрикам.
    Отныне и навсегда сайт акций и скидок в Алматы должен стать вашим лучшим другом и помощником! С нашей помощью вы сэкономите уйму денежных средств, невозвратимое время и дорогое здоровье. Удобное оформление сайта, правильно подобранная цветовая гамма, удобство в расположении рубрик не оставят равнодушным ни молодежь, ни людей преклонного возраста.
    Скидочные купоны в Алматы, представленные у нас — это гарантия настоящего качества. Мы ручаемся за каждого своего компаньона, уверены в безупречности каждого предложения, ведь все проверено лично нами! Если вы не успели применить скидку по назначению, или вас не устроило обслуживание — звоните по телефону, указанному на сайте, и наша служба заботы о пользователях обязательно даст вам все разъяснения. При возникновении вопросов или предложений также можно связаться с нами через форму обратной связи.</p>
                </div>
        </div>
        <div class="footer">
            <div class="row">
                <div class="col-md-2"><h4 style="color: white;">Компания</h4><hr><a>О chocolife.me</a></br><a>Пресса о нас</a></br><a>Работай с нами</a></br><a>Контакты</a></div>
                <div class="col-md-2"><h4 style="color: white;">Клиентам</h4><hr><a>Обратная связь</a></br><a>Обучающий видеоролик</a></br><a>Вопросы и ответы</a></br><a>Публичная оферта</a></div>
                <div class="col-md-2"><h4 style="color: white;">Партнерам</h4><hr><a>Для вашего бизнеса</a></div>
                <div class="col-md-4 col-md-offset-2"><h4 style="color: white;">Наше приложение</h4><hr><p style="color: white;">Chocolife.me теперь еще удобнее и всегда под рукой!</p>
                    <img src="images/googleplay.png" style="float: left;">
                    <img src="images/appstore.png" style="margin-left: 3px;">
                </div>
            </div>
            <hr>
            <div class="row">
                <p style="color: white;">Chocolife.me | 2011-2017 - <a>Карта сайта</a></p>
            </div>
        </div>
    </div>
</body>
</html>
@endsection