@extends('layouts.app')

@section('content')

	<style type="text/css">
        table tr td{
            padding: 15px;
        }
        table thead tr td{
            background: #ececec;
        }
        .pagination{
            margin: 25px 0 25px 0;
        }
        .pagination li{
            float: left;
            padding: 10px;
            margin-right: 15px;
        }
    </style>

    <div class="localnav-wrapper localnav-headless gh-show-below">
        <div class="localnav"></div>
    </div>

    <div id="page">
        <div class="container">
				<p><b>Question:</b> {{ $question->question }}</p>
				<form action="{{url('qa/answer/'.$question->id)}}" method="POST">
					{{ csrf_field() }}
					<textarea name="answer" id="" cols="30" rows="10"></textarea><br>
					<button type="submit">Answer!</button>
				</form>

			
		</div>
	</div>

@endsection