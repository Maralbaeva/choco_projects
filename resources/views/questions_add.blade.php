@extends('layouts.app')

@section('content')


	<style type="text/css">
        table tr td{
            padding: 15px;
        }
        table thead tr td{
            background: #ececec;
        }
        .pagination{
            margin: 25px 0 25px 0;
        }
        .pagination li{
            float: left;
            padding: 10px;
            margin-right: 15px;
        }
    </style>

    <div class="localnav-wrapper localnav-headless gh-show-below">
        <div class="localnav"></div>
    </div>

    <div id="page">
        <div class="container">
			

				<h1>Add question:</h1>

				<form action="{{ url('qa/add') }}" method="POST">
					{{ csrf_field() }}
					<input type="text" name="name" placeholder="Vvedite imya"> <br>
					<input type="text" name="email" placeholder="Vvedite email"><br>
					<p><input type="checkbox" name="ano"> Anonymous?</p>
					<textarea name="question" id="" cols="30" rows="10" placeholder="Vvedite vopros"></textarea><br>
					<button type="submit" style="background: #cecece; color: #000; padding: 10px;">Add question</button>
				</form>


		</div>
	</div>

@endsection