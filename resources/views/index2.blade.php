<?php
$connection = mysqli_connect('localhost', 'root', '', 'chocolife');
session_start();
$id=1;
$ses_sql = mysqli_query($connection,"select id, header, image, info, price from posts where id='$id'");
$row = mysqli_fetch_assoc($ses_sql);
$id_session = $row['id'];
$header_session = $row['header'];
$image_session = $row['image'];
$info_session = $row['info'];
$price_session = $row['price'];
?>

@extends('layouts.app')
@section('class') page-home @endsection
@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Cinemax</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
</head>
<body>
    <div>
        <nav class="navbar navbar-default">
            <div class="row">
              <div class="container-fluid col-md-5 col-md-offset-2">
                <ul class="nav navbar-nav menu">
                  <li class="active"><a href="index.php">Chocolife</a></li>
                  <li><a href="#">Chocomart</a></li>
                  <li><a href="#">Chocotravel</a></li>
                  <li><a href="#">Lensmark</a></li>
                  <li><a href="#">Chocofood</a></li>
                </ul>
              </div>
              <button type="button" class="btn btn-warning col-md-1 col-md-offset-1 reg">Регистрация</button>
              <h6 class="col-md-1 enter"><a>Вход</a></h6>
              <h6 class="col-md-1 enter"><a href="/add">Создать акцию</a></h6>

            </div>
        </nav>
        <div class="mai">
            <div class="row">
                <div class="col-md-3"><img src="images/705.png"></div>
                <div class="col-md-2"><h5>Главное,чтобы Вы </br> были счастливы!</h5></div>
                <div class="col-md-7">
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Найти среди 748 акций">
                        </div>
                    <button type="submit" class="btn btn-warning search"><i class="fa">Поиск</i></button>
                    </form>
                </div>
            </div>

            <div class="row">
            <hr>
                <nav class="menu"> 
                    <div class="col-md-offset-7" id="menu">
                        <ul class="third">
                            <li><a href="#">Все</a></li>
                            <li><a href="#">Новые</a></li>
                            <li><a href="#" style="color: red;">Хиты продаж</a></li>
                            <li><a href="#" style="color: black;"><b><ins>Развлечения и отдых</ins></b></a></li>
                            <li><a href="#">Красота и здоровье</a></li>
                            <li><a href="#">Спорт</a></li>
                            <li><a href="#">Товары</a></li>
                            <li><a href="#">Услуги</a></li>
                            <li><a href="#">Еда</a></li>
                            <li><a href="#">Туризм,отели</a></li>
                            <li><a href="#" style="color: green;"><b>Бесплатные купоны</b></a></li>
                            <hr>
                        </ul>
                    </div>
                </nav>
            </div>

            <div class="row submenu">
                <ul>
                    <li style="list-style: none;">Активный отдых</li>
                    <li>Бассейны</li>
                    <li>Караоке</li>
                    <li>Развлечения для детей</li>
                    <li>Парки развлечений</li>
                    <li style="color: black;"><b>Кинотеатры</b></li>
                    <li>Концерты и события</li>
                    <li style="list-style: none;">Театр им.Лермотова</li>
                    <li>Квесты,игровые клубы</li>
                    <li>Сауны,бани</li>
                </ul>
            </div>
            <button type="button" class="glav"><a href="index.html" style="text-decoration: none; color: black;">на главную страницу</a></button>
            
            <div class="panel panel-default">
                <div class="panel-heading"><p style="float: left; position: absolute; top: -2px;">Можно купить с 17 октября по 4 ноября</p><p style="position: absolute; right:13px; top: -2px;">Можно воспользоваться до 30 ноября 2017 года</p></div>
                <div class="panel-body">
                    <h4><?php echo $header_session; ?></h4>
                    <div class="row">
                        <div class="col-md-8">
                            <img src="images/23.jpg" style="position: relative; width: 100%; height: 20%;"></img>
                        </div>
                        <div class="col-md-4">
                            <h2><b>от <?php echo $price_session; ?> тг.</b></h2>
                            <h5 style="color: grey;">экономия от 600 тг.</h5>
                            <img src="images/buu.png"></img>
                            <hr>
                            <h5 style="color: grey;">Купили 1244 человека</h5>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default infor">
                <div class="panel-heading">
                    <ul>
                        <li><ins>Информация</ins></li>
                        <li>Отзывы(1194)</li>
                        <li><a href="/questions_add">Вопросы(22)</a></li>
                        <li>Получить 5000 тенге</li>
                        <li style="color: blue;">Как воспользоваться акцией</li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <ul>
                            <li><?php echo $info_session; ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="row">
                <div class="col-md-2"><h4 style="color: white;">Компания</h4><hr><a>О chocolife.me</a></br><a>Пресса о нас</a></br><a>Работай с нами</a></br><a>Контакты</a></div>
                <div class="col-md-2"><h4 style="color: white;">Клиентам</h4><hr><a>Обратная связь</a></br><a>Обучающий видеоролик</a></br><a>Вопросы и ответы</a></br><a>Публичная оферта</a></div>
                <div class="col-md-2"><h4 style="color: white;">Партнерам</h4><hr><a>Для вашего бизнеса</a></div>
                <div class="col-md-4 col-md-offset-2"><h4 style="color: white;">Наше приложение</h4><hr><p style="color: white;">Chocolife.me теперь еще удобнее и всегда под рукой!</p>
                    <img src="images/googleplay.png" style="float: left;">
                    <img src="images/appstore.png" style="margin-left: 3px;">
                </div>
            </div>
            <hr>
            <div class="row">
                <p style="color: white;">Chocolife.me | 2011-2017 - <a>Карта сайта</a></p>
            </div>
        </div>
    </div>
</body>
</html>
@endsection