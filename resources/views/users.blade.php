@extends('layouts.app')

@section('content')

    <style type="text/css">
        table tr td{
            padding: 15px;
        }
        table thead tr td{
            background: #ececec;
        }
        .pagination{
            margin: 25px 0 25px 0;
        }
        .pagination li{
            float: left;
            padding: 10px;
            margin-right: 15px;
        }
    </style>

    <div class="localnav-wrapper localnav-headless gh-show-below">
        <div class="localnav"></div>
    </div>

    <div id="page">
        <div class="container">
            <h1>Users</h1>
            <table border="1" style="width:100%;">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Email</td>
                        <td>Name</td>
                        <td>Created at</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->created_at->format('Y/m/d') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $users->links() }}
            <div style="clear: both;"></div>
        </div>
    </div>

@endsection